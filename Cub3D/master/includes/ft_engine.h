/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_engine.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 18:11:22 by thzeribi          #+#    #+#             */
/*   Updated: 2021/04/30 17:42:08 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ENGINE_H
# define FT_ENGINE_H
# include "libft.h"
# include "ft_parsing.h"
# include <stdio.h>
# include <stdlib.h>

# define C_GREEN "\033[1;32m"
# define C_RED "\033[1;31m"
# define C_RESET "\033[0m"

typedef struct s_engine
{
	t_params	*params;
}				t_engine;

#endif
