/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parsing.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/07 17:59:24 by thzeribi          #+#    #+#             */
/*   Updated: 2021/04/30 18:00:14 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PARSING_H
# define FT_PARSING_H
# include "libft.h"
# include <fcntl.h>
# include <stdio.h>


# define ERROR -1
# define TRUE 1
# define FALSE 0

typedef struct s_params
{
	int		res[2];
	int		*floor;
	int		*celing;
	char	*texture_no;
	char	*texture_sd;
	char	*texture_we;
	char	*texture_ea;
	int		nb_sprites;
	char	**sprites;
	int		save;
	int		params_sprite;
}				t_params;

t_params	*start_parsing(char *filename, int save, t_params *params);
void	get_resolution(t_params *params, const char *line);
char	*get_texture_path(const char *line);
char	**get_sprites_path(t_params *params, const char *line);
int		*get_color(const char *line);
void	check_nbr(char *nbr);
int		check_nbr_length(int nbr, int length);

#endif
