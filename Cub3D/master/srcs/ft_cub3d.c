/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cub3d.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 16:12:38 by thzeribi          #+#    #+#             */
/*   Updated: 2021/05/01 04:12:46 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_engine.h"

static int	check_file(char *filename)
{
	const char	*ext;

	ext = ft_file_ext(filename);
	if (ext != NULL && ft_strcmp(ext, "cub") == 0)
		return (TRUE);
	return (FALSE);
}

int	main(int argc, char *argv[])
{
	int	save;
	t_engine engine;
	t_params params;

	save = FALSE;
	if (argc <= 3)
	{
		if (check_file(argv[1]) == FALSE)
			print_error("Incorrect map !\n");
		else if (argc == 3)
		{
			if (strcmp(argv[2], "--save") != 0)
			{
				print_error("Invalid Arguments");
				return (ERROR);
			}
			save = TRUE;
		}
		engine.params = start_parsing(argv[1], save, &params);
		printf("\nNbr of sprite : %d\nResolution : [%d, %d]\n", engine.params->nb_sprites, params.res[0], params.res[1]);
		printf("Save : %s\n", (params.save == 1) ? "TRUE" : "FALSE");
		ft_putstr("\nAll params is good\nStarting Game\n");
	}
	else
		print_error("Invalid Arguments");
	return (0);
}
