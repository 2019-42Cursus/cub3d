/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/16 10:09:33 by thzeribi          #+#    #+#             */
/*   Updated: 2021/04/30 17:29:08 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_parsing.h"

void	get_resolution(t_params *params, const char *line)
{
	int		i;
	char	**str;

	i = 0;
	if (line == NULL)
		print_error("Error during parsing");
	while (line[++i])
		if (line[i] != ' ' && ft_isdigit(line[i]) == 0)
			print_error("Error during parsing");
	str = ft_split(line, ' ');
	if (str == NULL || str[1] == NULL || str[2] == NULL)
		print_error("Error during parsing");
	params->res[0] = ft_atoi(str[1]);
	params->res[1] = ft_atoi(str[2]);
}

int	*get_color(const char *line)
{
	int	*res;
	int i;
	char	**str;
	i = 0;
	
	if (line == NULL)
		print_error("Error during parsing");
	while (ft_isdigit(*line) != 1)
		line++;
	str = ft_split(line, ',');
	if (str == NULL || str[0] == NULL || str[1] == NULL || str[2] == NULL)
		print_error("Error during parsing");
	res = (int *)malloc(sizeof(int) * (3));
	while (str[i])
	{
		check_nbr(str[i]);
		if (check_nbr_length(ft_atoi(str[i]), 3))
			res[i] = ft_atoi(str[i]);
		i++;
	}
	if (res == NULL)
		print_error("Error during parsing");
	return (res);
}
