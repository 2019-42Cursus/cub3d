/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/19 11:41:00 by thzeribi          #+#    #+#             */
/*   Updated: 2021/04/30 18:01:56 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_parsing.h"

static t_params	init_default(t_params *params, int save)
{
	init_tab(params->res, 2);
	params->floor = NULL;
	params->celing = NULL;
	params->sprites = NULL;
	params->texture_ea = NULL;
	params->texture_no = NULL;
	params->texture_sd = NULL;
	params->texture_we = NULL;
	params->nb_sprites = 0;
	params->save = save;
	params->params_sprite = FALSE;
	return (*params);
}

static int	check_params(t_params *params)
{
	if (params->sprites == NULL || params->nb_sprites == 0)
		return (ERROR);
	else if (params->texture_ea == NULL)
		return (ERROR);
	else if (params->texture_no == NULL)
		return (ERROR);
	else if (params->texture_sd == NULL)
		return (ERROR);
	else if (params->texture_we == NULL)
		return (ERROR);
	else if (params->floor == NULL)
		return (ERROR);
	else if (params->celing == NULL)
		return (ERROR);
	return (TRUE);
}

static void	select_params(t_params *params, const char *line)
{
		if (line[0] == 'R' && line[1] == ' ')
			get_resolution(params, line);
		else if (line[0] == 'N' && line[1] == 'O')
			params->texture_no = get_texture_path(line);
		else if (line[0] == 'S' && line[1] == 'D')
			params->texture_sd = get_texture_path(line);
		else if (line[0] == 'W' && line[1] == 'E')
			params->texture_we = get_texture_path(line);
		else if (line[0] == 'E' && line[1] == 'A')
			params->texture_ea = get_texture_path(line);
		else if (line[0] == 'S' && line[1] == ' ')
			params->sprites = get_sprites_path(params, line);
		else if
		(params->params_sprite == TRUE && (line[0] == ' ' || line[0] == '\t'))
			params->sprites = get_sprites_path(params, line);
		else if (params->params_sprite == TRUE)
			params->params_sprite = FALSE;
		else if (line[0] == 'F' && line[1] == ' ')
			params->floor = get_color(line);
		else if (line[0] == 'C' && line[1] == ' ')
			params->celing = get_color(line);
}

t_params	*start_parsing(char *filename, int save, t_params *params)
{
	int			fd;
	char		*line;
	int			res;

	res = TRUE;
	init_default(params, save);
	fd = open(filename, O_RDONLY);
	if (fd == -1)
		print_error("Error during Parsing");
	while (get_next_line(fd, &line))
		select_params(params, line);
	close(fd);
	if (check_params(params) == ERROR)
		print_error("Error during Parsing");
	return (params);
}
