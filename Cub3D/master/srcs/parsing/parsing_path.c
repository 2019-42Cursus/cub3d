/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_path.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/21 18:00:34 by thzeribi          #+#    #+#             */
/*   Updated: 2021/04/28 19:42:52 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_parsing.h"

static char	*get_path(char *line)
{
	int		start;
	int		end;
	char	*path;

	start = 0;
	if (line == NULL)
		return (NULL);
	end = ft_strlen(line);
	if (line[end] == ' ')
		end--;
	path = ft_substr(line, start, end);
	if (!(path))
		return (NULL);
	return (path);
}

char	*get_texture_path(const char *line)
{
	char	**str;
	char	*path;

	path = NULL;
	if (line == NULL)
		print_error("Error during parsing");
	str = ft_split(line, ' ');
	if (str == NULL || str[1] == NULL)
		print_error("Error during parsing");
	path = get_path(str[1]);
	if (!path)
		print_error("Error during parsing");
	return (path);
}

char	**get_sprites_path(t_params *params, const char *line)
{
	int		i;
	char	**str;
	char	*path;
	char	**strs;

	i = 0;
	path = NULL;
	if (params->nb_sprites >= 8)
		print_error("To many sprites");
	if (line == NULL)
		print_error("Error during parsing");
	str = ft_split(line, ' ');
	if (str[i][0] == 'S' || str[i][0] == ' ')
		i++;
	if (str == NULL || str[0] == NULL)
		print_error("Error during parsing");
	path = get_path(str[i]);
	if (!path)
		print_error("Error during parsing");
	i = 0;
	strs = (char **)malloc(sizeof(char *) * (params->nb_sprites + 1));
	while (i < params->nb_sprites)
	{
		strs[i] = (char *)malloc(sizeof(char) * (ft_strlen(params->sprites[i]) + 1));
		strs[i] = params->sprites[i];
		i++;
	}
	strs[i] = (char *)malloc(sizeof(char) * (ft_strlen(path) + 1));
	strs[i] = path;
	params->nb_sprites += 1;
	params->params_sprite = TRUE;
	return (strs);
}
