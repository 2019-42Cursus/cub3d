/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nbr_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/30 17:09:17 by thzeribi          #+#    #+#             */
/*   Updated: 2021/04/30 17:25:24 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_engine.h"

void check_nbr(char *nbr)
{
	int i;

	i = 0;
	while (nbr[i])
	{
		if (ft_isdigit(nbr[i]) == 0)
		{
			if (nbr[i] == ' ' && (i == 0 || i == (int)ft_strlen(nbr) - 1))
			;
			else
				print_error("Error during parsing");
		}
		i++;
	}
}

int check_nbr_length(int nbr, int length)
{
	int count;

	count = 0;
	while (nbr != 0)
	{
		nbr /= 10;
		count++;
	}
	if (count > 0 && count <= length)
		return (TRUE);
	return (FALSE);
}